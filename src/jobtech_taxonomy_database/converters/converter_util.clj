(ns ^{:author "Batfish"
      :doc    "Utility functions for the jobtech-taxonomy-database project."}
 jobtech-taxonomy-database.converters.converter-util
  (:gen-class)
  (:require [datomic.client.api :as d]
            [jobtech-taxonomy-database.datomic-connection :as conn]))

(def get-entity-id-by-legacy-id-query
  '[:find ?s
    :in $ ?legacy-id ?type
    :where
    [?s :concept.external-database.ams-taxonomy-67/id ?legacy-id]
    [?s :concept/type ?type]])

(defn get-entity-id-by-legacy-id [legacy-id type]
  (ffirst (d/q get-entity-id-by-legacy-id-query (conn/get-db) (str legacy-id) type)))

(defn deprecate-concept [type legacy-id]
  {:pre [legacy-id type]}
  (if-let [entity-id (get-entity-id-by-legacy-id (str legacy-id) type)]
    {:db/id entity-id :concept/deprecated true}))
