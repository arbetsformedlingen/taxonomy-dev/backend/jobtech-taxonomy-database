(ns jobtech-taxonomy-database.api
  (:require [jobtech-taxonomy-database.create-database :as create-database]
            [jobtech-taxonomy-database.datomic-connection :as dc]
            [jobtech-taxonomy-database.config :as config]
            [clojure.string :as str]))

(defn create-db [{:keys [type]}]
  {:pre [(not (str/blank? type))]}
  (create-database/create type))

(defn setup-db [{:keys [name]}]
  {:pre [(not (str/blank? name))]}
  (println "Configuring Database" name)
  (binding [config/*db* name]
    (dc/init-new-db)
    (println "Done.")))

(comment

  (create-db {:type "dev"})

  (setup-db {:name "jobtech-taxonomy-dev-2021-06-15-10-17-36"})

  ,)