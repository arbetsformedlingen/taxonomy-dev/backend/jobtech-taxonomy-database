(ns jobtech-taxonomy-database.backup-database
  (:require
   [jobtech-taxonomy-database.config :as c]
   [wanderung.core :as w])
  (:gen-class))

(def base-folder-for-database-backups (str (System/getProperty "user.home") "/database-backup/"))

(defn database-folder-name [database-name]
  (str base-folder-for-database-backups database-name))

(defn get-datomic-config [datomic-database-name]
  (-> (:datomic-cfg (c/get-datomic-config))
      (assoc :name datomic-database-name)))

(defn get-datahike-config [datahike-database-name path-to-store-database-data]
  {:store {:backend :file
           :path path-to-store-database-data}
   :name datahike-database-name
   :schema-flexibility :write
   :keep-history? true})

(defn create-config [datomic-database-name]
  (let [datomic-config (get-datomic-config datomic-database-name)
        datahike-config (get-datahike-config datomic-database-name (database-folder-name datomic-database-name))]
    {:datomic-config datomic-config
     :datahike-config datahike-config}))

(defn check-if-database-backup-folder-already-exists [folder]
  (.exists (clojure.java.io/file folder)))

(defn create-database! [database-name]
  (let [{:keys [datomic-config datahike-config]} (create-config database-name)
        backup-folder (get-in datahike-config [:store :path])]

    (if  (check-if-database-backup-folder-already-exists backup-folder)
      (do
        (binding [*out* *err*]
          (println "Database folder " backup-folder  " already exists! Aborting program!!"))
        (System/exit 1))
      (do
        (println (str "Creating folder " backup-folder))
        (.mkdir (java.io.File. backup-folder))

        (println (str "Backuping database " database-name))
        (w/migrate [:datomic-cloud :datahike] datomic-config datahike-config)
        )
      )
    )
  )

;;TODO: WE NEED THE LATEST VERSION OF WANDERUNG IN CLOJARS!!
