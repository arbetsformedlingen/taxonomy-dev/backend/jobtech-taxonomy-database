(ns jobtech-taxonomy-database.config
  (:require [cprop.core :as c])
  (:gen-class))

;; TODO: load from separate, profile specific, properties files (dev/prod)
;;  ... (delay (load-file (.getFile (resource "config.clj"))))

(def ^:dynamic *db* nil)

(def config
  (if (= "PROD" (System/getenv "jobtechtaxonomy"))
    (c/load-config :file "config/prod/config.edn")
    (c/load-config :file "config/dev/config.edn")))

(def ^:private legacydb-config
  (:legacydb config))

(defn get-datomic-config []
  {:datomic-name *db*
   :datomic-cfg (:datomic-cfg config)})

(defn get-legacydb-config []
  legacydb-config)

(comment

  ;; useful for development in the REPL, instead of (binding [*db* "..."] ...)

  (alter-var-root #'*db* (constantly "jobtech-taxonomy-dev-..."))

  ,)