(ns jobtech-taxonomy-database.create-database
  (:require [datomic.client.api :as d]
            [jobtech-taxonomy-database.datomic-connection :as dc])
  (:import [java.text SimpleDateFormat]
           [java.util TimeZone Date]))

(def date-format
  (doto (SimpleDateFormat. "yyyy-MM-dd-HH-mm-ss")
    (.setTimeZone (TimeZone/getTimeZone "Europe/Berlin"))))

(defn get-timestamp []
  (.format date-format (Date.)))

(defn database-name [type]
  (str "jobtech-taxonomy-" type  "-" (get-timestamp)))

(defn create [type]
  (let [name (database-name type)]
    (println "Creating Database")
    (println name)
    (d/create-database (dc/get-client) {:db-name name})
    (println "Done.")
    name))