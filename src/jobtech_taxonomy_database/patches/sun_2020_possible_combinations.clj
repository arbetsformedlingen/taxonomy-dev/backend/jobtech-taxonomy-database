(ns jobtech-taxonomy-database.patches.sun-2020-possible-combinations
  (:gen-class)
  (:require [dk.ative.docjure.spreadsheet :as dox]

            [clojure.string :as s]
            [clojure.set :as st]

            [jobtech-taxonomy-database.types :as t]
            [jobtech-taxonomy-database.datomic-connection :as conn]
            [datomic.client.api :as d]

            [jobtech-taxonomy-common.relation :as relation]
            [jobtech-taxonomy-common.concept :as concept-common]))

;; TODO hard code education-level 00 not to have any relation to any field

(defn load-book []
  (dox/load-workbook "resources/kombinationer_level-field_sun2020.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-combinations []
  (dox/select-columns {:A :sun-education-level-2-code-2020
                       :B :sun-education-field-3-code-2020-impossible
                       :E :sun-education-field-3-code-2020-unlikely} (load-sheet "omöjliga kombinationer")))



(def query-sun-education-field-3
  '[:find (pull ?c [:concept/id
                    :concept/preferred-label
                    :concept/type
                    :concept.external-standard/sun-education-field-code-2020])
    :in $
    :where [?c :concept/type "sun-education-field-3"]
    (not [?c :concept/deprecated true])])


(defn sun-education-field-3 []
  (map first (d/q query-sun-education-field-3 (conn/get-db))))

(def query-sun-education-level-2
  '[:find (pull ?c [:concept/id
                    :concept/preferred-label
                    :concept/type
                    :concept.external-standard/sun-education-level-code-2020])
    :in $
    :where [?c :concept/type "sun-education-level-2"]
    (not [?c :concept/deprecated true])])


(def query-sun-education-level-2-test
  '[:find (pull ?r [* {:relation/concept-1 [:concept/id
                                            :concept/preferred-label
                                            :concept.external-standard/sun-education-field-code-2020
                                            :concept.external-standard/sun-education-level-code-2020]
                       :relation/concept-2 [:concept/id
                                            :concept/preferred-label
                                            :concept.external-standard/sun-education-field-code-2020
                                            :concept.external-standard/sun-education-level-code-2020]}])
    :in $
    :where [?c :concept/type "sun-education-level-2"]
    (not [?c :concept/deprecated true])
    [?r :relation/concept-1 ?c]
    [?r :relation/concept-2 ?c2]
    [?r :relation/type "unlikely-combination"]
    [?c :concept.external-standard/sun-education-level-code-2020 "31"]
    [?c2 :concept.external-standard/sun-education-field-code-2020 "580"]])

(defn sun-education-field-3 []
  (map first (d/q query-sun-education-field-3 (conn/get-db))))



(defn sun-education-level-2 []
  (map first (d/q query-sun-education-level-2 (conn/get-db))))

(defn reduce-on-key-fun [k]
  (fn [acc element]
    (assoc acc (get element k) element)))

(defn sun-education-field-3-map []
  (reduce (reduce-on-key-fun :concept.external-standard/sun-education-field-code-2020) {} (sun-education-field-3)))

(defn sun-education-level-2-map []
  (reduce (reduce-on-key-fun :concept.external-standard/sun-education-level-code-2020) {} (sun-education-level-2)))

(defn csv->list [csv]
  (map s/trim (s/split csv #",")))

(defn handle-an-unlikely-combination [row]
  (let [education-level-2-codes (csv->list (:sun-education-level-2-code-2020 row))
        education-level-2-concepts (map #(get (sun-education-level-2-map) %) education-level-2-codes)

        education-field-3-codes-unlikely (csv->list (:sun-education-field-3-code-2020-unlikely  row))

        education-field-3-unlikely-concepts (map #(get (sun-education-field-3-map) %) education-field-3-codes-unlikely)]

    (for [el2 education-level-2-concepts
          ef3 education-field-3-unlikely-concepts]
      [el2 ef3])))



(defn handle-an-impossible-combination [row]
  (let [education-level-2-codes (csv->list (:sun-education-level-2-code-2020 row))
        education-level-2-concepts (map #(get (sun-education-level-2-map) %) education-level-2-codes)

        education-field-3-codes-impossible (remove empty? (csv->list (:sun-education-field-3-code-2020-impossible row)))

        education-field-3-impossible-concepts (map
                                               (fn [suncode]
                                                 (let [concept (get (sun-education-field-3-map) suncode)]
                                                   (when (nil? concept)
                                                     (throw (Exception. (str "Concept cant be nil! "  suncode))))
                                                   concept))
                                               education-field-3-codes-impossible)]



    (for [el2 education-level-2-concepts
          ef3 education-field-3-impossible-concepts]
      [el2 ef3])))

(defn daycare-related-to-all-fields []
  {:sun-education-level-2-code-2020 "00",
   :sun-education-field-3-code-2020-impossible (reduce (fn [acc element] (str acc element ", ")) "" (map :concept.external-standard/sun-education-field-code-2020 (sun-education-field-3)))})

(defn handle-impossible-combinations []
  (mapcat handle-an-impossible-combination
          (cons (daycare-related-to-all-fields)
                (take 3 (drop 3 (load-combinations))))))

(defn handle-unlikely-combinations []
  (mapcat handle-an-unlikely-combination (take 2 (drop 4 (load-combinations)))))

(defn all-concept-combinations []
  (for [eduction-level (sun-education-level-2)
        eduction-field (sun-education-field-3)]
    [eduction-level eduction-field]))

(defn all-possible-concept-combinations []
  (st/difference (st/difference (set (map vec (all-concept-combinations)))
                                (set (handle-impossible-combinations)))
                 (set (handle-unlikely-combinations))))

;; Create new copy of prod database
;; TODO create import to datomic transact data

(defn create-relation [level-id field-id r]
  (relation/edge-tx level-id r field-id))

(defn combinations->tx [combinations rel]
  (map (fn [c]
         (create-relation (:concept/id (first c)) (:concept/id (second c))  rel)) combinations))

(defn unlikely-combinations-tx []
  (combinations->tx (handle-unlikely-combinations) t/unlikely-combination))

(defn possible-combinations-tx []
  (combinations->tx (all-possible-concept-combinations) t/possible-combination))


(defn transact-possible-combinations-relations []
  (d/transact (conn/get-conn)
              {:tx-data (possible-combinations-tx)}))

(defn transact-unlikely-combinations-relations []
  (d/transact (conn/get-conn)
              {:tx-data (unlikely-combinations-tx)}))
