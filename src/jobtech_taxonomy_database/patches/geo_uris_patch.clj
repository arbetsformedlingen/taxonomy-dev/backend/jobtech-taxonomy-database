(ns jobtech-taxonomy-database.patches.geo_uris_patch
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [datomic.client.api :as d]
            [jobtech-taxonomy-database.datomic-connection :as conn]))


(def countries-with-uris
  (next
    (with-open [reader (io/reader "resources/countries-urls.csv")]
      (doall
        (csv/read-csv reader)))))


(def municipalities-with-uris
  (next (map (fn [[uri code]]
               {:concept.external-standard/wikidata-uri    uri
                :concept.external-standard/lau-2-code-2015 code})
             (with-open [reader (io/reader "resources/municipalities.csv")]
               (doall
                 (csv/read-csv reader))))))


(def regions-with-uris
  (next (map (fn [[uri code]]
               {:concept.external-standard/wikidata-uri                    uri
                :concept.external-standard/national-nuts-level-3-code-2019 code})
             (with-open [reader (io/reader "resources/regions-swe.csv")]
               (doall
                 (csv/read-csv reader))))))


(def taxonomy-municipalities
  (into {}
        (map (juxt (fn [[{code :concept.external-standard/lau-2-code-2015}]]
                     code)
                   (fn [[{id :concept/id}]]
                     id))
             (d/q
               '[:find (pull ?c [:concept/id
                                 :concept/preferred-label
                                 :concept/type
                                 :concept.external-standard/lau-2-code-2015])
                 :where [?c :concept.external-standard/lau-2-code-2015]]
               (conn/get-db)))))


(def taxonomy-regions
  (into {}
        (map (juxt (fn [[{code :concept.external-standard/national-nuts-level-3-code-2019}]]
                     code)
                   (fn [[{id :concept/id}]]
                     id))
             (d/q
               '[:find (pull ?c [:concept/id
                                 :concept/preferred-label
                                 :concept/type
                                 :concept.external-standard/national-nuts-level-3-code-2019])
                 :where [?c :concept.external-standard/national-nuts-level-3-code-2019]]
               (conn/get-db)))))


(defn- add-uris []
  (concat
    (map (fn [municipality]
           (assoc municipality :db/id [:concept/id
                                       (taxonomy-municipalities
                                         (:concept.external-standard/lau-2-code-2015 municipality))]))
         municipalities-with-uris)
    (map (fn [region]
           (assoc region :db/id [:concept/id
                                 (taxonomy-regions
                                   (:concept.external-standard/national-nuts-level-3-code-2019 region))]))
         regions-with-uris)
    (map (fn [[q-id _ _ _ iso-uri id]]
           {:db/id                                  [:concept/id id]
            :concept.external-standard/iso-uri      iso-uri
            :concept.external-standard/wikidata-uri q-id})
         countries-with-uris)))


(defn- patch []
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Lägger till extern URI för begreppet."}]
                              (add-uris))}))
