(ns jobtech-taxonomy-database.patches.ontology-patcher
  (:gen-class)
  (:require
    [clojure.data.csv :as csv]
    [clojure.java.io :as io]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.nano-id :as nano-id]
    [jobtech-taxonomy-common.relation :as relation]
    [clojure.string :as s]
    [jobtech-taxonomy-database.datomic-connection :as conn]))

"Takes csv with two values per row:
  - preferred-label for new keyword
  - concept-id for occupation name concept to set relation to

 Creates new keyword concepts and relations to occupation-name concepts
 if a relation does not already exist between specified concepts"

(def load-csv
  (with-open [reader (io/reader "resources/ontology-l.csv")]
    (doall
      (csv/read-csv reader
                    :separator \;))))


(def ontology-terms-and-taxonomy-ids
  ;; TODO check for headings?
  (map (fn [[label id]]
         ;; remove zwnbsp-characters:
         [(s/replace label "﻿" "") id])
       load-csv))


(def occupation-ids-in-taxonomy
  (mapv (fn [[{id :concept/id}]] id)
       (d/q
         '[:find (pull ?c [:concept/id
                           :concept/preferred-label
                           :concept/type])
           :where
           [?c :concept/type "occupation-name"]]
         (conn/get-db))))


(def keyword-labels-and-ids-in-taxonomy
  (mapv #(select-keys (first %) [:concept/id :concept/preferred-label])
        (d/q
          '[:find (pull ?c [:concept/id
                            :concept/preferred-label
                            :concept/type])
            :where [?c :concept/type "keyword"]]
          (conn/get-db))))


(def keyword-occupation-name-relations-in-taxonomy
  (map (comp (juxt (comp :concept/preferred-label :relation/concept-2)
                   (comp :concept/id :relation/concept-1)) first)
       (d/q
         '[:find (pull ?r [{:relation/concept-1 [:concept/id
                                                 :concept/preferred-label
                                                 :concept/type]
                            :relation/concept-2 [:concept/id
                                                 :concept/preferred-label
                                                 :concept/type]}])
           :in $
           :where [?r :relation/concept-1 ?c-1]
           [?r :relation/concept-2 ?c-2]
           (or (and [?c-1 :concept/type "occupation-name"]
                    [?c-2 :concept/type "keyword"])
               (and [?c-1 :concept/type "keyword"]
                    [?c-2 :concept/type "occupation-name"]))]
         (conn/get-db))))


(defn- id-exists? [id]
  ; Checks if an (occupation-name) id exists in the taxonomy.
  (some #(= id %) occupation-ids-in-taxonomy))


(defn- label-exists? [label]
  ; Checks if (keyword) label exists in the taxonomy.
  (some #(= label (:concept/preferred-label %)) keyword-labels-and-ids-in-taxonomy))


(defn- no-relation-exists? [relation]
  ; Takes vector with two values: preferred-label for keyword and id for occupation-name.
  ; Checks if no relation between specified label and id exists in the taxonomy.
  (not-any? #(= relation %) keyword-occupation-name-relations-in-taxonomy))


(defn- find-kw-id [label]
  ; Takes preferred-label for keyword-concept, returns its id.
  ((into {} (map (juxt :concept/preferred-label
                       :concept/id) keyword-labels-and-ids-in-taxonomy)) label))


(defn- create-concepts-and-relations [data]
  ; Takes collection of label-id pairs.
  ; Validates input label-id combo and creates new concept and/or relation if everything looks good.
  (let [new-concepts (map (fn [pref-label]
                            (when (nil? (label-exists? pref-label))
                              {:db/id                   (str pref-label "-" "keyword")
                               :concept/id              (nano-id/generate-new-id-with-underscore)
                               :concept/preferred-label pref-label
                               :concept/definition      pref-label
                               :concept/type            "keyword"}))
                          (distinct (map first data)))

        labels-and-ids (into {} (map (juxt :db/id
                                           :concept/id)) new-concepts)

        relations (map (fn [[pref-label occupation-id]]
                         (when (id-exists? occupation-id)
                           (if (label-exists? pref-label)
                             (when (no-relation-exists? [pref-label occupation-id])
                               ; if keyword concept exists, but not with the supplied relation
                               (relation/edge-tx (find-kw-id pref-label)
                                                 "related"
                                                 occupation-id))
                             ; new concepts in this transaction
                             (relation/edge-tx (labels-and-ids (str pref-label "-" "keyword"))
                                               "related"
                                               occupation-id
                                               {::relation/concept-1-tempid (str pref-label "-" "keyword")}))))
                       data)]
    (into [] (comp cat (remove nil?)) [new-concepts relations])))


(defn- info []
  ; Run this before patching. Checks for potential issues in the supplied data.
  (remove nil?
          (map (fn [[label id]]
                 (if-not (id-exists? id)
                   (println "Concept-id for the occupation in the following combination does not exist:" [label id])
                   (when (label-exists? label)
                     (if (no-relation-exists? [label id])
                       (println "Label for combination exists but is not related to target keyword - new relation will be set from existing occupation:" [label id])
                       (println "The following relation already exists and will not be created:" [label id])))))
               ontology-terms-and-taxonomy-ids)))


(defn- patch []
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Nytt begrepp från synonymordlistan."}]
                              (create-concepts-and-relations ontology-terms-and-taxonomy-ids))}))

