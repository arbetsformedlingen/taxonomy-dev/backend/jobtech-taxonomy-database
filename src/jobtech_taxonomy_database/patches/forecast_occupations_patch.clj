(ns jobtech-taxonomy-database.patches.forecast-occupations-patch
  (:require
    [dk.ative.docjure.spreadsheet :as dox]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.converters.nano-id-assigner :as nano]
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [jobtech-taxonomy-database.types :as t]
    [jobtech-taxonomy-common.relation :as relation]))

"
Adds concepts with type forecast-occupation. Sets relations to SSYK groups.
A forecast occupation can be related to one ore more SSYK-level-4 groups.

Done in two transactions in which (1) the concepts are created and (2) relations set.
Queries will be done on the created concepts' labels to find ids for the relations.
"

(defn load-book []
  (dox/load-workbook "resources/yp.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-forecast-occupations []
  (dox/select-columns {:A :forecast-occupation
                       :B :definition}
                      (load-sheet "forecast-occupation")))

(defn load-relations []
  (dox/select-columns {:A :ssyk-group-id
                       :B :forecast-occupation-label}
                      (load-sheet "relations")))

(defn define-relations [ssyk-level-4-id forecast-occupation]
  (relation/edge-tx ssyk-level-4-id t/related forecast-occupation))

(defn define-forecast-occupation [forecast-label forecast-definition]
  (let [concept-id (nano/get-nano "forecast-occupation" forecast-label)]
    {:db/id                                            concept-id
     :concept/id                                       concept-id
     :concept/type                                     "forecast-occupation"
     :concept/preferred-label                          forecast-label
     :concept/definition                               forecast-definition}))

(def query
  '[:find (pull ?c [:concept/id
                    :concept/preferred-label
                    :concept/type])
    :in $ ?label
    :where [?c :concept/type "forecast-occupation"]
    [?c :concept/preferred-label ?label]])

(defn get-forecast-occupation-id [label]
  "Takes string and checks if preferred-label exists in db"
  (ffirst (d/q query (conn/get-db) label)))

(defn create-forecast-occupation [row]
  "Takes list of keyword preferred labels and occupation-name concept ids.
  If label does not exists in any of existing keywords in db -> create new keyword and set relations"
  (let [label (clojure.string/trim (:forecast-occupation row))
        definition (:definition row)]
    (define-forecast-occupation label definition)))

(defn create-relations [row]
  "Takes list outlining relations per row and queries db to get id for the newly created
  forecast-occupation concept. Sets relation"
  (let [ssyk-level-4-id (:ssyk-group-id row)
        forecast-occupation-id (get-forecast-occupation-id (:forecast-occupation-label row))]
    (define-relations ssyk-level-4-id forecast-occupation-id)))

"Run transacts in order."
(defn transact-concepts []
  (d/transact (conn/get-conn)
              {:tx-data (map create-forecast-occupation (rest (load-forecast-occupations)))}))

(defn transact-relations []
  (d/transact (conn/get-conn)
              {:tx-data (map create-relations (rest (load-relations)))}))

