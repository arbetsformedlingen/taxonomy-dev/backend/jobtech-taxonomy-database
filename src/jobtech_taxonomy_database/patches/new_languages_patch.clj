(ns jobtech-taxonomy-database.patches.new-languages-patch
  (:gen-class)
  (:require
    [jobtech-taxonomy-database.types :as t]
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [datomic.client.api :as d]))

(def new-languages
  [{:concept/id                               "bcwy_vdR_ePV"
    :concept/preferred-label                  "Nyöstsyriska"
    :concept/definition                       "Nyöstsyriska"
    :concept/type                             t/language
    :concept.external-standard/iso-639-3-2007 "AII"}

   {:concept/id                               "QdWc_H2x_CN9"
    :concept/preferred-label                  "Turoyo"
    :concept/definition                       "Turoyo"
    :concept/type                             t/language
    :concept.external-standard/iso-639-3-2007 "TRU"}])


(defn patch []
  (d/transact (conn/get-conn)
              {:tx-data new-languages}))
