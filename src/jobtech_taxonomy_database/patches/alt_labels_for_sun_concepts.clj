(ns jobtech-taxonomy-database.patches.alt-labels-for-sun-concepts
  (:require
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]))

"The API returns an exception when trying to add alternative labels to
these concepts. Likely because of definition collision."

(defn patch-sun-alt-labels []
  (d/transact (conn/get-conn)
              {:tx-data
               (into [{:db/id           "datomic.tx"
                       :daynote/comment "Alternativ benämning läggs till."}]
                     ; alt label for new sun gymnasium concept
                     [{:concept/id                 "wyLL_K3a_HRC"
                       :concept/alternative-labels ["Gymnasium eller motsvarande med examen"]}
                      ; alt label for old sun gymnasium concept
                      {:concept/id                 "DeBt_ahh_bkx"
                       :concept/alternative-labels ["Gymnasium eller motsvarande med examen"]}])}))