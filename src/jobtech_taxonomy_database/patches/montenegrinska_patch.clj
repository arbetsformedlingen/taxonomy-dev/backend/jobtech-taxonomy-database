(ns jobtech-taxonomy-database.patches.montenegrinska-patch
  (:gen-class)
  (:require
    [jobtech-taxonomy-database.types :as t]
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [datomic.client.api :as d]))

(def new-language
  [{:concept/id  "kBKA_GoN_CrX"
    :concept/preferred-label "Montenegrinska"
    :concept/definition "Montenegrinska"
    :concept/type t/language
    :concept.external-standard/iso-639-2-1998 "CNR"
    :concept.external-standard/iso-639-3-2007 "CNR"}])

(defn patch []
  (d/transact (conn/get-conn)
              {:tx-data new-language}))
