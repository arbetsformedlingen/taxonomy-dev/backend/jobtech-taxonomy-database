(ns jobtech-taxonomy-database.patches.esco-skill-mappings
  (:require
    [dk.ative.docjure.spreadsheet :as dox]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [jobtech-taxonomy-common.relation :as relation]
    [jobtech-taxonomy-database.nano-id :as nano-id]))

; NEW TYPE -> UPDATE TYPES NS AS WELL

(defn load-book []
  (dox/load-workbook "resources/esco-skills-mappings.xlsx"))

(def book (memoize load-book))

(defn load-concepts []
  (dox/select-sheet "Sheet1" (book)))

(defn create-concepts-and-relations []
  (let [new-esco-concepts (map (fn [{esco-uri   :esco-uri
                                     esco-label :esco-pref-label}]
                                 {:db/id                              esco-uri
                                  :concept/id                         (nano-id/generate-new-id-with-underscore)
                                  :concept/type                       "esco-skill"
                                  :concept/definition                 esco-label
                                  :concept/preferred-label            esco-label
                                  :concept.external-standard/esco-uri esco-uri})
                               ; Get unique uri-and-label combos for concept creation
                               (distinct (rest (dox/select-columns {:A :esco-uri
                                                                    :B :esco-pref-label}
                                                                   (load-concepts)))))
        ; Create mappings for id-lookup for relation creation
        uris-and-ids (into {} (map (juxt :concept.external-standard/esco-uri
                                         :concept/id)) new-esco-concepts)

        relations (map (fn [{esco-uri      :esco-uri
                             concept-id    :taxo-skill-id
                             relation-type :relation-type}]
                         (relation/edge-tx (uris-and-ids esco-uri)
                                           relation-type
                                           concept-id
                                           {::relation/concept-1-tempid esco-uri}))

                       ; Each row a relation
                       (rest (dox/select-columns {:A :esco-uri
                                                  :C :relation-type
                                                  :D :taxo-skill-id}
                                                 (load-concepts))))]

    (concat new-esco-concepts relations)))

(defn patch []
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Mappning till ESCO-kompetens importerad."}]
                              (create-concepts-and-relations))}))


