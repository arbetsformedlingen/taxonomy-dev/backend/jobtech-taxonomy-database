(ns jobtech-taxonomy-database.patches.add-uris-for-new-esco-concepts
  (:gen-class)
  (:require
    [clojure.data.csv :as csv]
    [clojure.java.io :as io]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]))


(defn- load-csv [file]
  ; all esco-occupations from esco website
  (with-open [reader (io/reader file)]
    (doall
      (csv/read-csv reader))))

(def esco-skills-and-occupations
  (merge (into {}
               (map (fn [[_ uri _ label]]
                      [label uri])
                    (load-csv "resources/occupations_sv.csv")))
         (into {}
               (map (fn [[_ uri _ _ label]]
                      [label uri])
                    (load-csv "resources/skills_sv.csv")))))

(def esco-concepts-in-taxonomy-missing-uris
  (d/q
    '[:find (pull ?c [:db/id
                      :concept/id
                      :concept/preferred-label
                      :concept/type
                      :concept.external-standard/esco-uri
                      :concept/deprecated])
      :in $
      :where

      [(missing? $ ?c :concept.external-standard/esco-uri)]
      [(missing? $ ?c :concept/deprecated)]
      (or [?c :concept/type "esco-skill"]
          [?c :concept/type "esco-occupation"])]
    (conn/get-db)))


(def duplicates
  ; preferred-labels for some esco concepts are reoccuring
  ; these must be handled manually since we do not know which uri to use
  ; based only on the label
  (remove nil?
          (map (fn [[k v]] (when (> v 1) k))
               (frequencies
                 (into []
                       (map (fn [[{label :concept/preferred-label}]] label)
                            esco-concepts-in-taxonomy-missing-uris))))))


(defn- add-missing-uris []
  ; if uri is missing - use pref. label to look it up in the
  ; map containing all esco-occupation labels and their uris
  (map (fn [[concept]]
         (cond-> concept
                 (not-any? #(= (:concept/preferred-label concept) %) duplicates)
                 (assoc :concept.external-standard/esco-uri
                        (esco-skills-and-occupations (:concept/preferred-label concept)))))
       esco-concepts-in-taxonomy-missing-uris))


(defn- patch []
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Lägger till ESCO-uri."}]
                              (add-missing-uris))}))
