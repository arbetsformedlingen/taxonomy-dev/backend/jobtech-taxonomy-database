(ns jobtech-taxonomy-database.patches.esco-unmapped-patch
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [datomic.client.api :as d]
            [jobtech-taxonomy-database.datomic-connection :as conn]
            [jobtech-taxonomy-database.nano-id :as nano-id]))

(def all-esco-skills
  (with-open [reader (io/reader "resources/skills_sv.csv")]
    (doall
      (csv/read-csv reader))))


(def get-esco-skill-uris-from-taxonomy
  (into []
        (map (fn [[{uri :concept.external-standard/esco-uri}]] uri)
             (d/q
               '[:find (pull ?c [:concept/id
                                 :concept/preferred-label
                                 :concept/type
                                 :concept.external-standard/esco-uri])
                 :where
                 [?c :concept/type "esco-skill"]]
               (conn/get-db)))))


(defn add-esco-skills []
  (remove nil?
          (map (fn [[_ uri _ _ label]]
                 ;; add only new concepts when uri does not already exist in the taxonomy
                 (if (not-any? #(= uri %) get-esco-skill-uris-from-taxonomy)
                   {:concept/id                         (nano-id/generate-new-id-with-underscore)
                    :concept/preferred-label            label
                    :concept/definition                 label
                    :concept/type                       "esco-skill"
                    :concept.external-standard/esco-uri uri}))
               (next all-esco-skills))))


(defn patch []
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id "datomic.tx"
                               :daynote/comment "Importerar ESCO-kompetens."}]
                              (add-esco-skills))}))
