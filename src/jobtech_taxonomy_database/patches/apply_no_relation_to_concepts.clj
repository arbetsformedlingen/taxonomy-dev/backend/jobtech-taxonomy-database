(ns jobtech-taxonomy-database.patches.apply-no-relation-to-concepts
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [datomic.client.api :as d]
            [jobtech-taxonomy-database.datomic-connection :as conn]))

(def no-relation-concepts
  (with-open [reader (io/reader "resources/no_esco_relation_concepts.csv")]
    (doall
      (csv/read-csv reader
                    :separator \;))))

(def no-relation-uris
  (map second (rest no-relation-concepts)))

(def esco-skills-in-db
  (d/q
    '[:find (pull ?c [:concept/id
                      :concept/type
                      :concept/preferred-label
                      :concept.external-standard/esco-uri
                      :concept/deprecated])
      :in $
      :where [?c :concept/type "esco-skill"]
             [(missing? $ ?c :concept/deprecated)]]
    (conn/get-db)))

(defn query->map-coll
  [q]
  (map first q))

(defn associate-no-esco-relation-attrib
  [coll]
  (map #(assoc % :concept/no-esco-relation true) coll))


(defn- patch
  []
  (let [coll (filter (fn [c] (some #(= (:concept.external-standard/esco-uri c) %) no-relation-uris))
                     (query->map-coll esco-skills-in-db))
        patched-concepts (associate-no-esco-relation-attrib coll)]
    (d/transact (conn/get-conn)
                {:tx-data
                 (into [{:db/id           "datomic.tx"
                         :daynote/comment "Begreppet kan inte kopplas till någon kompetens i Taxonomy."}]
                       patched-concepts)})))

(comment
  (patch))
