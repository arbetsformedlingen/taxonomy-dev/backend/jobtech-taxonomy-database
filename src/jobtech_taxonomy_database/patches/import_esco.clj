(ns jobtech-taxonomy-database.patches.import-esco
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as s]
            [clojure.string :as str]
            [datomic.client.api :as d]
            [jobtech-taxonomy-common.relation :as relation]
            [jobtech-taxonomy-database.config :as config]
            [jobtech-taxonomy-database.datomic-connection :as conn]
            [jobtech-taxonomy-database.nano-id :as nano-id]
            [jobtech-taxonomy-common.db-schema :as db-schema]))

(def ^:private headers
  #{["Classification 1 URI"
     "Classification 1 PrefLabel"
     "Classification 1 URL"
     "Classification 2 ID"
     "Classification 2 PrefLabel"
     "Classification 2 URL"
     "Mapping relation"
     "Editorial note"]})

(defn- conform! [spec x]
  (let [ret (s/conform spec x)]
    (if (s/invalid? ret)
      (throw (ex-info "Invalid" (s/explain-data spec x)))
      ret)))

(defn- load-esco-csv []
  (with-open [reader (io/reader (io/resource "export v3 ESCO - corrected.csv"))]
    (->> (csv/read-csv reader)
         (conform!
           (s/cat
             :skip (s/* (complement headers))
             :headers headers
             :rows (s/+ (s/spec
                          (s/cat :esco-uri string?
                                 :esco-label string?
                                 :esco-url string?
                                 :taxonomy-id string?
                                 :taxonomy-label string?
                                 :taxonomy-url string?
                                 :relation #{"no relation"
                                             "unmapped"
                                             "skos:broadMatch"
                                             "skos:closeMatch"
                                             "skos:exactMatch"
                                             "skos:narrowMatch"}
                                 :note string?)))))
         :rows
         (map #(dissoc % :taxonomy-url :esco-url))
         (remove #(-> % :esco-uri #{""}))
         (map #(condp (fn [substr str] (str/starts-with? str substr))
                      (:esco-uri %)
                 "http://data.europa.eu/esco/isco/C"
                 (assoc % :type :isco
                          :isco (subs (:esco-uri %)
                                      (count "http://data.europa.eu/esco/isco/C")))

                 "http://data.europa.eu/esco/occupation/"
                 (assoc % :type :occupation))))))

(defn- esco-txs []
  (let [db (conn/get-db)
        rows (load-esco-csv)
        {:keys [isco occupation]} (group-by :type rows)
        isco->id (into {}
                       (d/q
                         '{:find [?isco ?id]
                           :in [$ [?isco ...]]
                           :where [[?c :concept.external-standard/isco-code-08 ?isco]
                                   [?c :concept/id ?id]]}
                         db
                         (map :isco isco)))
        new-occupations (->> occupation
                             (map (juxt :esco-uri :esco-label))
                             distinct
                             (map (fn [[esco-uri esco-label]]
                                    {:db/id esco-uri
                                     :concept/id (nano-id/generate-new-id-with-underscore)
                                     :concept/type "esco-occupation"
                                     :concept/preferred-label esco-label
                                     :concept/definition esco-label
                                     :concept.external-standard/esco-uri esco-uri})))
        esco-uri->new-concept-id (->> new-occupations
                                      (map (juxt :concept.external-standard/esco-uri
                                                 :concept/id))
                                      (into {}))]
    (concat
      [{:db/id "datomic.tx"
        :daynote/comment "Import ESCO concepts"}]
      ;; only add metadata to isco concepts
      (->> isco
           (map (juxt :isco :esco-uri))
           distinct
           (map (fn [[isco esco-uri]]
                  {:db/id esco-uri
                   :concept/id (isco->id isco)
                   :concept.external-standard/esco-uri esco-uri})))
      ;; create esco occupations
      new-occupations
      ;; relations
      (->> rows
           (map #(update % :relation {"skos:broadMatch" "broad-match"
                                      "skos:closeMatch" "close-match"
                                      "skos:exactMatch" "exact-match"
                                      "skos:narrowMatch" "narrow-match"}))
           (filter :relation)
           (map #(relation/edge-tx (esco-uri->new-concept-id (:esco-uri %))
                                   (:relation %)
                                   (:taxonomy-id %)
                                   {::relation/concept-1-tempid (:esco-uri %)}))))))

(defn- apply! [{:keys [db-name]}]
  (binding [config/*db* db-name]
    (d/transact (conn/get-conn) {:tx-data db-schema/schema})
    (d/transact (conn/get-conn) {:tx-data (esco-txs)})))

(comment

  (apply! {:db-name "jobtech-taxonomy-vlaaad-2021-06-29-14-31-02"})

  ;; e.g.
  ; clj -X jobtech-taxonomy-database.patches.import-esco/apply! :db-name '"jobtech-taxonomy-vlaaad-2021-06-29-14-31-02"'

  ,)