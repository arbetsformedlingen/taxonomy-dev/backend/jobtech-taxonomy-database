(ns jobtech-taxonomy-database.patches.nuts-2021-patcher
  (:gen-class)
  (:require
    [dk.ative.docjure.spreadsheet :as dox]
    [jobtech-taxonomy-database.types :as t]
    [datomic.client.api :as d]
    [jobtech-taxonomy-common.relation :as relation]
    [jobtech-taxonomy-database.converters.converter-util :as u]
    [jobtech-taxonomy-database.converters.nano-id-assigner :as nano]
    [jobtech-taxonomy-database.datomic-connection :as conn]))



;; patch deprecates existing region-type concepts (except those belonging to sweden) and creates new concepts for
;; the new regions, in order to implement the updated nuts 2021 standard.
;; new attribute denoting the new nuts code series will also be added


(defn load-book []
  (dox/load-workbook "resources/all-regions.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-regions []
  (dox/select-columns {:A :nuts-2021-code :B :region-preferred-label :C :broader-related-id} (load-sheet "regions")))

(defn load-swedish-regions []
  (dox/select-columns {:A :nuts-2021-code :B :region-preferred-label :C :concept-id} (load-sheet "swe-regions")))

;; new nuts schema needs to be added to jobtech-taxonomy-common/db-schema ns as well
(def nuts-2021-schema
  [{:db/ident       :concept.external-standard/nuts-level-3-code-2021
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc         "NUTS-2021 level 3 code"}])

(defn create-region [region-preferred-label nuts-code]
  (let [concept-id (nano/get-nano "region" region-preferred-label)]
    {:db/id                                            concept-id,
     :concept/id                                       concept-id,
     :concept/type                                     t/region,
     :concept/definition                               region-preferred-label
     :concept/preferred-label                          region-preferred-label
     :concept.external-standard/nuts-level-3-code-2021 nuts-code}))

;; swedish regions are to be excluded from the deprecation:
(def nuts-query '[:find (pull ?c [:concept/id
                                  :concept/preferred-label
                                  :concept/deprecated
                                  :concept.external-database.ams-taxonomy-67/id
                                  :concept/type
                                  :concept.external-standard/nuts-level-3-code-2013])
                  :in $
                  :where [?c :concept/type "region"]
                  [?c :concept.external-standard/nuts-level-3-code-2013 ?nuts]
                  (not [(.contains ?nuts "SE")])])

;; some of the old concepts do not have the nuts-level-3-code attribute but should still be deprecated:
(def missing-query '[:find (pull ?c [:concept/id
                                     :concept/preferred-label
                                     :concept/deprecated
                                     :concept.external-database.ams-taxonomy-67/id
                                     :concept/type
                                     :concept.external-standard/nuts-level-3-code-2013])
                     :in $
                     :where [?c :concept/type "region"]
                     [(missing? $ ?c :concept.external-standard/nuts-level-3-code-2013)]
                     [?c :concept/preferred-label ?label]

                     ;; concept is newly created and does not have legacy id - will be excluded from this patch:
                     (not [(.contains ?label "Ospecifierad")])

                     ;; exclude any newly created regions as fail-safe:
                     (not [?c :concept.external-standard/nuts-level-3-code-2021])])


(defn create-broader-relation [region-id country-id region-tempid]
  (relation/edge-tx region-id t/broader country-id
                    {::relation/concept-1-tempid region-tempid}))

(defn create-concepts-and-relations [row]
  (let [region (create-region (:region-preferred-label row)
                              (:nuts-2021-code row))
        relations (create-broader-relation (:concept/id region) (:broader-related-id row) (:db/id region))]
    (concat [region] [relations])))


(defn deprecate-old-nuts [old-nuts-region]
  (u/deprecate-concept (:concept/type old-nuts-region)
                       (:concept.external-database.ams-taxonomy-67/id old-nuts-region)))


;; take concept id from excel and use as db/id
(defn add-db-id [concept]
  (assoc concept :db/id [:concept/id (:concept-id concept)]))

(defn add-nuts-2021-codes-to-swe-concept [concept]
  (assoc (add-db-id concept) :concept.external-standard/nuts-level-3-code-2021
                             (:nuts-2021-code concept)))


;; get rid of unwanted keys from excel
(defn toss-keys [m]
  (apply dissoc m [:region-preferred-label :concept-id :nuts-2021-code]))


(defn patch-nuts []
  (d/transact (conn/get-conn)
              {:tx-data nuts-2021-schema})

  (d/transact (conn/get-conn)
              {:tx-data (map deprecate-old-nuts (flatten (concat (d/q nuts-query (conn/get-db))
                                                                 (d/q missing-query (conn/get-db)))))})
  (d/transact (conn/get-conn)
              {:tx-data (mapcat create-concepts-and-relations (rest (load-regions)))})

  ;; new attribute need to be added to existing swedish region-type concepts:
  (d/transact (conn/get-conn)
              {:tx-data (map toss-keys (map add-nuts-2021-codes-to-swe-concept (rest (load-swedish-regions))))}))