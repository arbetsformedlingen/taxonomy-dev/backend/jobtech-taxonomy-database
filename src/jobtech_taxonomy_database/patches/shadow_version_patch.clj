(ns jobtech-taxonomy-database.patches.shadow-version-patch
  (:require
    [clj-http.client :as client]
    [jobtech-taxonomy-common.relation :as relation]
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [datomic.client.api :as d]
    [clojure.set :as set]))

"The aim of this patch is to create a version in which a certain type of concept is reverted to a previous version's
 configurement. Labels and relations that occurs in both versions should be as they were in the previous
 version (if changed). Only concepts as they were in the previous version and concepts that have been added since
 should exist in the shadow version."

(def graphql-endpoints
  {:local "http://localhost:3000/v1/taxonomy/graphql?query="})

(defn query-graph-ql
  "Takes queried data and does a bit of un-nesting."
  [q endpoint & api-key]
  (-> (client/get (str (endpoint graphql-endpoints) q)
                  {:headers (when api-key {:api-key api-key})
                   :as      :json})
      (get-in [:body :data :concepts])))

(def v-next-occupations
  (query-graph-ql
    "query MyQuery {
  concepts(type: \"occupation-name\", version: \"next\", include_deprecated: true) {
    id
    preferred_label
    type
    deprecated
    replaced_by {
      id
    }
    broader (type: \"ssyk-level-4\") {
      id
    }
  }
}
"
    :local
    "222"))

(def v-1-occupations
  (query-graph-ql
    "query MyQuery {
    concepts(type: \"occupation-name\", version: \"1\", include_deprecated: true) {
      id
      preferred_label
      type
      deprecated
      replaced_by {
        id
        }
      broader (type: \"ssyk-level-4\") {
        id
      }
    }
  }
  "
    :local))

(def v-1-ssyk-and-field
  (query-graph-ql
    "query MyQuery {
    concepts(version: \"1\", type: \"ssyk-level-4\") {
      id
      preferred_label
      broader(type: \"occupation-field\") {
        id
        preferred_label
      }
    }
  }
  "
    :local))

(def v-next-ssyk-and-field
  (query-graph-ql
    "query MyQuery {
    concepts(version: \"next\", type: \"ssyk-level-4\") {
      id
      preferred_label
      broader(type: \"occupation-field\") {
        id
        preferred_label
      }
    }
  }
  " :local "222"))

(defn ids-and-labels
  "Creates map of id:label or id:id (for broader label)
   for each concept in coll."
  ([concepts]
   (into {} (map (juxt :id :preferred_label)) concepts))
  ([concepts relation]
   (into {} (map (juxt :id (comp :id (fn [c] (first (relation c)))))) concepts)))

(defn get-broader-concepts
  "Useful for when having to compare occupation-field
   concepts between versions without creating additional queries."
  [coll]
  (distinct (map (fn [c] (-> c
                             :broader
                             first)) coll)))

(defn find-deprecated
  "Takes a coll of version specific concepts
   (deprecated attribute included). Ids should
   be a flat coll of ids from version 1."
  [ids v-maps]
  (filterv (fn [c] (some #(= (:id c) %) ids))
           (filter (fn [c] (:deprecated c)) v-maps)))

(defn find-updates
  "Returns map of concepts subjected to
   changes between two versions. Use with-relation
   when checking for relation changes."
  [coll-1 coll-2 & relation]
  (into {} (filter (fn [[_ v]] (and (seq? v)
                                    (< 1 (count v))))
                   (merge-with (comp distinct vector)
                               (if relation
                                 (ids-and-labels coll-1 (first relation))
                                 (ids-and-labels coll-1))
                               (if relation
                                 (ids-and-labels coll-2 (first relation))
                                 (ids-and-labels coll-2))))))

(defn resurrector
  "Rids afflicted concepts of deprecation status."
  [coll]
  (map (fn [{:keys [id]}] [:db/retract [:concept/id id] :concept/deprecated]) coll))

(defn label-reverter
  "Reverts label to state in previous version."
  [[k v]]
  (assoc {:db/id [:concept/id k]} :concept/preferred-label (first v)))

(defn find-relation-entity
  "Gets relation db/ids for retraction."
  [c-1 relation-type c-2]
  (ffirst
    (d/q '{:find  [?r]
           :in    [$ ?id]
           :where [[?r :relation/id ?id]]}
         (conn/get-db)
         (relation/id c-1 relation-type c-2))))

(defn retract-relation
  "If relation has changed between versions - retract."
  [[occupation-id related-ids]]
  [:db/retractEntity (find-relation-entity (second related-ids) "narrower" occupation-id)])

(defn reconnect-relation
  "Reconnects relation from previous version."
  [[occupation-id ssyk-ids]]
  (relation/edge-tx (first ssyk-ids) "narrower" occupation-id))

(defn retract-attrib
  [attrib id-1 val]
  [:db/retract [:concept/id id-1] attrib [:concept/id val]])

(defn retract-replaced-by
  []
  (map (partial retract-attrib :concept/replaced-by)
       (map first (find-updates v-1-occupations v-next-occupations :replaced_by))
       (map (comp second second) (find-updates v-1-occupations v-next-occupations :replaced_by))))

(defn transact-changes
  "Takes collection from resurrect, label-revert,
   retract and reconnect functions."
  [tx-coll]
  (d/transact
    (conn/get-conn)
    {:tx-data tx-coll}))

(defn iter-seq
  "Use for transforming iterable from tx-range fn into seq.
   Stolen from: https://stackoverflow.com/questions/9225948/how-do-turn-a-java-iterator-like-object-into-a-clojure-sequence"
  ([iterable]
   (iter-seq iterable (.iterator iterable)))
  ([iterable i]
   (lazy-seq
     (when (.hasNext i)
       (cons (.next i) (iter-seq iterable i))))))

(defn get-tx-log
  "Get transaction log for supplied t:values."
  [start end]
  (iter-seq (d/tx-range (conn/get-conn) {:start start
                                         :end   end})))

(defn rollback
  "Based on https://stackoverflow.com/questions/25389807/how-do-i-undo-or-reverse-a-transaction-in-datomic

    Reassert retracted datoms and retract asserted datoms in a transaction,
    effectively \"undoing\" the transaction.

    WARNING: *very* naive function!

    Takes a map/datomic-transaction-log."
  [tx-log]
  (let [transaction (:data tx-log)
        tx-id ((comp :tx first) transaction) ; get the transaction entity id
        new-data (->> transaction   ; get the datoms from the transaction
                      (remove #(= (:e %) tx-id)) ; remove transaction-metadata datoms
                      ; invert the datoms add/retract state.
                      (map #(do [(if (:added %)
                                   :db/retract
                                   :db/add)
                                 (:e %) (:a %) (:v %)]))
                      reverse)] ; reverse order of inverted datoms.
    (transact-changes new-data)))


(defn- patch!
  "Applies all transactions specified below."
  []
  (doall (map transact-changes
              ; For occupation-name concepts:
              [(map label-reverter (find-updates v-1-occupations
                                                 v-next-occupations))
               (resurrector (find-deprecated (keys (ids-and-labels v-1-occupations))
                                             v-next-occupations))
               (map retract-relation (find-updates v-1-occupations
                                                   v-next-occupations
                                                   :broader))
               (map reconnect-relation (find-updates v-1-occupations
                                                     v-next-occupations
                                                     :broader))
               (retract-replaced-by)
               ; For ssyk-level-4 and occupation-field concepts:
               (map label-reverter (find-updates (get-broader-concepts v-1-ssyk-and-field)
                                                 (get-broader-concepts v-next-ssyk-and-field)))
               (map retract-relation (find-updates v-1-ssyk-and-field
                                                   v-next-ssyk-and-field
                                                   :broader))
               (map reconnect-relation (find-updates v-1-ssyk-and-field
                                                     v-next-ssyk-and-field
                                                     :broader))])))
  
(comment
  ; Before creating the shadow version, test below should
  ; generate an empty list if all is correct
  (set/difference (set v-1-occupations) (set v-next-occupations))
  ; the one below should only show new concepts if all is correct
  (set/difference (set v-next-occupations) (set v-1-occupations)))


