(ns jobtech-taxonomy-database.patches.sort-order-skill-headline
  (:gen-class)
  (:require
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [datomic.client.api :as d]
    [dk.ative.docjure.spreadsheet :as dox]))

(defn load-book []
  (dox/load-workbook "resources/skill-headline-sort-order.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-skill-headlines []
  (dox/select-columns {:C :sort-order :D :id} (load-sheet "Sheet1")))

(defn add-db-id [concept]
  (assoc concept :db/id [:concept/id (:id concept)]))

(defn add-sort-order [concept]
  (assoc (add-db-id concept) :concept/sort-order (int (:sort-order concept))))

(defn toss-keys [m]
  (dissoc m :sort-order :id))

(defn patch []
  (d/transact (conn/get-conn)
              {:tx-data (map toss-keys (map add-sort-order (rest (load-skill-headlines))))}))


