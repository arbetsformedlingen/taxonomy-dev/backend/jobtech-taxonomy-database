(ns jobtech-taxonomy-database.patches.skat-patcher
  (:require [jobtech-taxonomy-database.datomic-connection :as conn]
            [datomic.client.api :as d]))

;; creates three new concepts for unemployment-codes (SKAT)
;; updates sort-order for old unemployment-type concepts (should be ascending, same as the codes)

(def new-unemployment-codes
  [{:concept/id "jLJd_D9N_KbZ",
   :concept/definition "Etableringsprogrammet, kartläggning",
   :concept/preferred-label "Etableringsprogrammet, kartläggning",
   :concept/type "unemployment-type",
   :concept/sort-order 28,
   :concept.external-standard/unemployment-type-code "28"}

  {:concept/id "oexD_LMe_scs",
   :concept/definition "Introduktionsjobb",
   :concept/preferred-label "Introduktionsjobb",
   :concept/type "unemployment-type",
   :concept/sort-order 30,
   :concept.external-standard/unemployment-type-code "30"}

  {:concept/id "pCT2_7N2_prN",
   :concept/definition "Etableringsprogrammet",
   :concept/preferred-label "Etableringsprogrammet",
   :concept/type "unemployment-type",
   :concept/sort-order 68,
   :concept.external-standard/unemployment-type-code "68"}])

(def skat-query '[:find (pull ?c [:concept/id
                                  :concept/type
                                  :concept/sort-order
                                  :concept/preferred-label
                                  :concept.external-standard/unemployment-type-code])
                  :in $
                  :where [?c :concept/type "unemployment-type"]])

(defn add-db-id [concept]
  (assoc concept :db/id [:concept/id (:concept/id concept)]))

(defn update-sort-order [concepts]
  (assoc (add-db-id concepts) :concept/sort-order (Long/valueOf (:concept.external-standard/unemployment-type-code concepts))))

(defn update-sort-order-skat-collection []
  (map update-sort-order (flatten (d/q skat-query (conn/get-db)))))

(defn patch-skat-changes []
  (d/transact (conn/get-conn) {:tx-data new-unemployment-codes})
  (d/transact (conn/get-conn) {:tx-data (update-sort-order-skat-collection)}))
