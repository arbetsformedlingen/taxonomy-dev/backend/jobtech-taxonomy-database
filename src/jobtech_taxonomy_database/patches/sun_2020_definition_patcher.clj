(ns jobtech-taxonomy-database.patches.sun-2020-definition-patcher
  (:gen-class)
  (:require
    [dk.ative.docjure.spreadsheet :as dox]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]))


;; patch adds the sun-2020 definitions to existing sun-2020 concepts specified in scb:s documentation
;; supplied xlsx-file includes ids for affected concepts

(defn load-book []
  (dox/load-workbook "resources/suncode-definitions.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-definitions []
  (dox/select-columns {:A :concept-id :C :sun-2021-definition} (load-sheet "Sheet1")))

(defn add-db-id [concept]
  (assoc concept :db/id [:concept/id (:concept-id concept)]))

(defn add-definitions [concepts]
  (assoc (add-db-id concepts) :concept/definition  (:sun-2021-definition concepts)))


;; get rid of unwanted keys from file:
(defn toss-keys [m]
  (apply dissoc m [:concept-id :sun-2021-definition]))

(defn patch-sun-2020-definitions []
  (d/transact (conn/get-conn)
              {:tx-data (map toss-keys (map add-definitions (rest (load-definitions))))}))


