(ns jobtech-taxonomy-database.patches.skill-alt-labels-patch
  (:require
    [dk.ative.docjure.spreadsheet :as dox]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]))

"Imports alternative labels from excel to db. IDs for concept in one column, alt-labels in another."

(defn load-book []
  (dox/load-workbook "resources/skill-alt-labels.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-alt-labels []
  (dox/select-columns {:B :skill-id :C :alt-label} (load-sheet "samtliga")))

(defn add-db-id [concept]
  (assoc concept  :db/id [:concept/id (:skill-id concept)]))

(defn add-alt-labels [concept]
  (assoc (add-db-id concept) :concept/alternative-labels (:alt-label concept)))

(defn toss-keys [m]
  (dissoc m :skill-id :alt-label))

(defn patch []
  (d/transact (conn/get-conn)
              {:tx-data (map toss-keys (map add-alt-labels (rest (load-alt-labels))))}))

