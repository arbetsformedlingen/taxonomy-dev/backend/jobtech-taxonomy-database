(ns jobtech-taxonomy-database.patches.eures-nace-code-patcher
  (:gen-class)
  (:require
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [datomic.client.api :as d]
    [dk.ative.docjure.spreadsheet :as dox]
    [jobtech-taxonomy-common.db-schema :as db-schema]))

; TODO the new attribute needs to be added to graphql concept ns in api project.

(defn load-book []
  (dox/load-workbook "resources/sni-eures-nace-all.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-nace-codes []
  (dox/select-columns {:B :eures-nace-code :C :concept-id} (load-sheet "Sheet1")))

(defn add-db-id [concept]
  (assoc concept :db/id [:concept/id (:concept-id concept)]))

(defn add-nace-codes [concept]
  (assoc (add-db-id concept) :concept.external-standard/eures-nace-code-2007 (:eures-nace-code concept)))

(defn toss-keys [m]
  (dissoc m :eures-nace-code :concept-id))

(defn patch []
  (d/transact (conn/get-conn)
              {:tx-data db-schema/schema})
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Lägger till NACE-koder enligt EURES-format."}]
                              (map (comp toss-keys add-nace-codes) (rest (load-nace-codes))))}))


