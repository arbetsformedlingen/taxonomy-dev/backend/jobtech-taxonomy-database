(ns jobtech-taxonomy-database.patches.esco-language-mappings
  (:require [datomic.client.api :as d]
            [jobtech-taxonomy-common.relation :as relation]
            [jobtech-taxonomy-database.datomic-connection :as conn]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]))

(defn load-csv
  [file]
  (with-open [reader (io/reader file)]
    (doall
      (csv/read-csv reader))))

(def mappings
  (load-csv "resources/esco_language_mappings.csv"))

(def esco-skills
  (d/q
    '[:find (pull ?c [:concept/id
                      :concept/preferred-label
                      :concept/type
                      :concept.external-standard/esco-uri])
      :in $
      :where [?c :concept/type "esco-skill"]
      [(missing? $ ?c :concept/deprecated)]]
    (conn/get-db)))

(def label-id-mappings
  (into {}
        (map (comp (fn [c] [(:concept.external-standard/esco-uri c)
                            (:concept/id c)]) first)
             esco-skills)))

(defn create-mappings
  [[uri _ mapping _ _ concept-id]]
  (if (= "exact-match" mapping)
    (relation/edge-tx concept-id
                      mapping
                      (label-id-mappings uri))
    (relation/edge-tx (label-id-mappings uri)
                      mapping
                      concept-id)))

(defn- patch
  []
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Mappning mellan taxonomy-språk och ESCO-språk."}]
                              (map create-mappings (next mappings)))}))

(comment
  ;; contents of the transaction
  (map create-mappings (next mappings)))






