(ns jobtech-taxonomy-database.patches.add-no-esco-relation-schema
  (:require
   [datomic.client.api :as d]
   [jobtech-taxonomy-database.datomic-connection :as conn]
   [jobtech-taxonomy-common.db-schema :as schema]
   [jobtech-taxonomy-database.config :as config]))

;schema version  
(defn- patch []
  (alter-var-root
   #'config/*db*
   (constantly "jobtech-taxonomy-prod-write-2022-11-08-15-20-01"))
  
  (d/transact (conn/get-conn) {:tx-data schema/schema}))

(comment

  (patch))