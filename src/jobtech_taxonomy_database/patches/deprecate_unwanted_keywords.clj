(ns jobtech-taxonomy-database.patches.deprecate-unwanted-keywords(:gen-class)
  (:require
    [dk.ative.docjure.spreadsheet :as dox]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]))

;; Some keyword preferred labels overlap with occupation-name concepts' alternative labels.
;; The keyword concepts are regarded as duplicates and will be deprecated with this patch.

(defn load-book []
  (dox/load-workbook "resources/compare-kw-alt-labels.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-keyword []
  (dox/select-columns {:B :kw-id} (load-sheet "Sheet1")))

(defn add-db-id [concept]
  (assoc concept :db/id [:concept/id (:kw-id concept)]))

(defn deprecate-kw-concepts [concepts]
  (assoc (add-db-id concepts) :concept/deprecated (:concept/deprecated concepts true)))

(defn toss-keys [m]
  (dissoc m :kw-id))

(defn patch []
  (d/transact (conn/get-conn)
                {:tx-data (map toss-keys (map deprecate-kw-concepts (rest (load-keyword))))}))
