(ns jobtech-taxonomy-database.patches.update-forecast-occupations
  (:gen-class)
  (:require
    [clojure.data.csv :as csv]
    [clojure.java.io :as io]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [jobtech-taxonomy-database.nano-id :as nano-id]
    [jobtech-taxonomy-common.relation :as relation]
    [jobtech-taxonomy-database.nano-id :as nano]))


(def updated-forecast-occupations
  (with-open [reader (io/reader "resources/yrken_tax_id.csv")]
    (doall
      (csv/read-csv reader :separator \;))))


(def forecast-occupations-in-taxonomy
  (flatten
    (d/q
      '[:find (pull ?r [{:relation/concept-1 [:concept/id
                                              :concept/preferred-label
                                              :concept/type
                                              :concept.external-standard/ssyk-code-2012]
                         :relation/concept-2 [:concept/id
                                              :concept/preferred-label
                                              :concept/type]}])
        :in $
        :where [?r :relation/concept-1 ?c-1]
        [?r :relation/concept-2 ?c-2]
        [?c-1 :concept/type "ssyk-level-4"]
        [?c-2 :concept/type "forecast-occupation"]]
      (conn/get-db))))


(def old-labels
  (map #(-> %
            :relation/concept-2 :concept/preferred-label)
       forecast-occupations-in-taxonomy))


(def current-relations
  (map vector
    (map #(-> %
              :relation/concept-1 :concept.external-standard/ssyk-code-2012)
         forecast-occupations-in-taxonomy)
    (map #(-> %
              :relation/concept-2 :concept/preferred-label)
         forecast-occupations-in-taxonomy)))


(def get-forecast-id-from-label
  (zipmap
    (map #(-> %
              :relation/concept-2 :concept/preferred-label) forecast-occupations-in-taxonomy)
    (map #(-> %
              :relation/concept-2 :concept/id) forecast-occupations-in-taxonomy)))


(def name-changes
  ;; {forecast-occupation-id: new-label}
  {"Ffqb_8Mq_Yth" "VVS-montörer"
   "jSSk_UqT_bgo" "Utredare och kvalificerade handläggare"
   "ES2d_RDA_JHj" "Systemanalytiker och IT-arkitekter"
   "1Uj2_6JM_zuz" "Supporttekniker inom IT"
   "mqze_D9J_Sgx" "Skötare inom psykiatrisk vård"
   "UsoN_wah_kh4" "Processövervakare inom kemisk industri"
   "nF3a_Y5a_3KB" "Drifttekniker inom IT"
   "T4Ws_RzG_WYF" "Grafiska formgivare"
   "dPDC_KMd_dHY" "Controllers"
   "4bxB_tig_Z7j" "Informatörer, kommunikatörer och PR-specialister"})


(def concepts-to-deprecate-and-replace
  ;; {id-to-deprecate: label-for-new-replacing-concept}
  {"GrTd_Afn_YS3" "Butikssäljare inom fackhandel",
   "pdU9_Kkf_JkF" "Butikssäljare inom dagligvaruhandel",
   "3mkk_7Kw_Uwf" "Designer och utvecklare inom spel och digitala medier",
   "A3b2_jkn_9Ek" "Designer och utvecklare inom spel och digitala medier",
   "9ZML_2tB_QyC" "Undersköterskor"
   "HN1d_Uyu_Bm9" "Undersköterskor"
   "pSEL_8po_cxx" "Socialsekreterare och biståndsbedömare"
   "9jMc_5xr_mWc" "Restaurang- och kafébiträden"
   "UWuk_qEo_hd7" "Processoperatörer inom tillverkning"
   "AWt8_idw_DYJ" "Odlare av trädgårds- och jordbruksväxter samt djuruppfödare blandad drift"
   "QTLk_iy2_dwQ" "Montörer inom tillverkning"
   "BSsf_9pX_Y8a" "Montörer inom tillverkning"
   "oyHH_nJg_qDz" "Maskinoperatörer inom tillverkning",
   "N2RL_3Cc_B6H" "Maskinoperatörer inom tillverkning"
   "f1D5_74v_Cni" "Maskinoperatörer inom tillverkning"
   "rqpF_M2S_cfV" "Maskinoperatörer inom tillverkning"
   "xpi3_7cz_gGf" "Maskinoperatörer inom tillverkning"
   "Rz1N_242_tnY" "Maskinoperatörer inom tillverkning"
   "LqD7_i7w_Lx5" "Maskinoperatörer inom tillverkning"
   "sUQk_zYU_jHT" "Maskinoperatörer inom tillverkning"
   "5UUG_TVT_zg2" "Maskinoperatörer inom tillverkning"
   "sCv2_DPu_Uvo" "Maskinoperatörer inom tillverkning"
   "yvvw_6Nj_Rab" "Kockar, köksmästare och souschefer"
   "fMWa_acR_NJz" "Restaurang- och kafébiträden"
   "1j8K_e3W_9f9" "Socialsekreterare och biståndsbedömare"
   "ggD9_m3t_8if" "Butikssäljare inom dagligvaruhandel"})


(defn- apply-updates []
  (let [update-labels (map (fn [[id label]]
                             {:db/id                   [:concept/id id]
                              :concept/preferred-label label}) name-changes)
        new-concepts (->> updated-forecast-occupations
                          (map #(second %))
                          next
                          distinct
                          (map (fn [label] (when (and (not-any? #(= label %) old-labels)
                                                      (not-any? #(= label %) (vals name-changes)))
                                             {:db/id                   (str label "-" "forecast-occupation")
                                              :concept/id              (nano-id/generate-new-id-with-underscore)
                                              :concept/preferred-label label
                                              :concept/definition      label
                                              :concept/type            "forecast-occupation"})))
                          (remove nil?))
        label-id-map (into {} (map (juxt :concept/preferred-label
                                         :concept/id) new-concepts))
        temp-id-map (into {} (map (juxt :db/id
                                        :concept/id) new-concepts))
        deprecate (->> forecast-occupations-in-taxonomy
                      (map #(-> % :relation/concept-2))
                      (into {} (map (juxt :concept/preferred-label
                                          :concept/id)))
                      (map (fn [[k v]] (when (and (not-any? #(= k %) (map #(second %) updated-forecast-occupations))
                                                  (not-any? #(= v %) (keys name-changes))
                                                  (not-any? #(= v %) (keys concepts-to-deprecate-and-replace)))
                                         {:db/id [:concept/id v]
                                          :concept/deprecated true})))
                      (remove nil?))

        deprecate-and-replace (map (fn [[id label]]
                         {:db/id               [:concept/id id]
                          :concept/deprecated  true
                          :concept/replaced-by (str label "-" "forecast-occupation")})
                       concepts-to-deprecate-and-replace)

        relations (->> updated-forecast-occupations
                       next
                       (map (fn [[ssyk-code label ssyk-id]]
                              (when (and (not-any? #(= [ssyk-code label] %) current-relations)
                                         (not-any? #(= label %) (vals name-changes)))
                                [ssyk-code label ssyk-id])))
                       (remove nil?)
                       (map (fn [[_ label ssyk-id]]
                              ;; for new concepts
                              (if (some #(= label %) (keys label-id-map))
                                (relation/edge-tx ssyk-id
                                                  "related"
                                                  (temp-id-map (str label "-" "forecast-occupation"))
                                                  {::relation/concept-2-tempid (str label "-" "forecast-occupation")})
                                ;; when concepts already exist in the taxonomy
                                (relation/edge-tx ssyk-id
                                                  "related"
                                                  (get-forecast-id-from-label label))))))]
    (concat update-labels
            new-concepts
            deprecate
            deprecate-and-replace
            relations)))


(defn- patch []
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Uppdaterar prognosyrke efter beställning från Analysavdelningen."}]
                              (apply-updates))}))


