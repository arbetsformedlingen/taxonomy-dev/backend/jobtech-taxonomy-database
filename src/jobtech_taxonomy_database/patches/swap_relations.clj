(ns jobtech-taxonomy-database.patches.swap-relations
  (:require [datomic.client.api :as d]
            [jobtech-taxonomy-common.relation :as relation]
            [jobtech-taxonomy-database.datomic-connection :as conn]))

"for symmetrical relations"

(def query
  (d/q
    '[:find (pull ?r [:relation/id
                      :relation/type
                      {:relation/concept-1 [:concept/id
                                            :concept/preferred-label
                                            :concept/type]
                       :relation/concept-2 [:concept/id
                                            :concept/preferred-label
                                            :concept/type]}])
      :in $
      :where [?r :relation/concept-1 ?c-1]
      [?r :relation/concept-2 ?c-2]
      (or
        (and
          [?r :relation/type "related"]
          [?c-1 :concept/type "keyword"]
          (or [?c-2 :concept/type "occupation-name"]
              [?c-2 :concept/type "skill"]))
        (and
          (or
            [?r :relation/type "exact-match"]
            [?r :relation/type "close-match"])
          (or
            (and
              [?c-1 :concept/type "esco-occupation"]
              [?c-2 :concept/type "occupation-name"])
            (and
              [?c-1 :concept/type "esco-skill"]
              [?c-2 :concept/type "skill"]))))]
    (conn/get-db)))

(defn swap-concepts
  "Relation direction corrections. Takes coll from query.
   Note that old relation ids are actually in the correct form
   and thus will be overwritten with an identical id. But even though
   the old ids looked correct, the relations themselves were in fact
   pointing in the wrong direction."
  [[{:keys [relation/concept-1 relation/concept-2 relation/type]}]]
  (relation/edge-tx (:concept/id concept-2)
                    type
                    (:concept/id concept-1)))


(defn transact-swapped-maps []
  (d/transact
    (conn/get-conn)
    {:tx-data (into [{:db/id           "datomic.tx"
                      :daynote/comment "Relation felriktad, vänds."}]
                    (map swap-concepts query))}))

;; when run again after reloading ns, 'query' should be empty


;; No need to retract old relation ids since they will be overwritten, code below not needed
#_
(def relation-ids
  "Relation-ids set from wrong direction"
  (map (comp :relation/id first)
       query))
#_
(defn retract-bad-relations []
  ;; not needed
  (d/transact
    (conn/get-conn)
    {:tx-data
     (map (fn [id] [:db/retractEntity [:relation/id id]])
          relation-ids)}))