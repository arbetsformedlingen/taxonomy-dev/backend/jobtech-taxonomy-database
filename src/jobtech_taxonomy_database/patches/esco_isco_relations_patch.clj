(ns jobtech-taxonomy-database.patches.esco-isco-relations-patch
  (:require
    [dk.ative.docjure.spreadsheet :as dox]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [jobtech-taxonomy-database.types :as t]
    [jobtech-taxonomy-common.relation :as relation]))

(defn load-book []
  (dox/load-workbook "resources/esco-isco-mappings.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-esco-and-isco []
  (dox/select-columns {:E :esco-occupation-id
                       :D :isco-level-4-id}
                      (load-sheet "Sheet1")))

(defn define-relations [row]
  (relation/edge-tx (:esco-occupation-id row)
                    t/broader
                    (:isco-level-4-id row)))

(defn patch []
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Relation från ESCO-yrke till ISCO-grupp."}]
                              (map define-relations (rest (load-esco-and-isco))))}))