(ns jobtech-taxonomy-database.patches.patch-duplicate
  (:gen-class)
  (:require
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [datomic.client.api :as d]))


"The concept Personaldirektör 79uB_oJR_dSQ was deprecated by mistake and later on a duplicate, FZ8o_uu3_WzS, was created.
Both concepts shall be deprecated and replaced by y22E_Nz6_5VU."



(defn patch-personaldirektor []
  (d/transact (conn/get-conn)
              {:tx-data

               [{:db/id              [:concept/id "79uB_oJR_dSQ"]
                 :concept/deprecated true
                 :concept/replaced-by {:db/id [:concept/id "y22E_Nz6_5VU"]}}

                {:db/id               [:concept/id "FZ8o_uu3_WzS"]
                 :concept/deprecated  true
                 :concept/replaced-by {:db/id [:concept/id "y22E_Nz6_5VU"]}}

                {:db/id           "datomic.tx"
                 :daynote/comment "Personaldirektör har duplicerats. Båda concept ska avaktualiseras och få replaced-by HR-Chef y22E_Nz6_5VU."}]}))
