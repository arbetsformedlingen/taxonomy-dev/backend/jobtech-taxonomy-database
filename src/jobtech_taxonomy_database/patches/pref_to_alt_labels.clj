(ns jobtech-taxonomy-database.patches.pref-to-alt-labels
  "Add alternative labels for concepts that have slashes in their labels"
  (:require [jobtech-taxonomy-database.datomic-connection :as conn]
            [datomic.client.api :as d]
            [clojure.string :as str]))

(defn- default-split
  "Split label on every slash character

  Example: \"Foo/Bar/Baz\" => [\"Foo\" \"Bar\" \"Baz\"]"
  [label]
  (map str/trim (str/split label #"/")))

(defn- split-concat
  "Concat words surrounding first slash character with remaining words

  Example:
  \"Analys/Beräkning-FEM-Design\" => [\"Analys-FEM-Design\" \"Beräkning-FEM-Design\"]"
  [label]
  (let [[before-slash [_ alt-2 & suffix]] (->> label
                                               (re-seq #"\s*(/)\s*|[a-zA-ZäÄöÖåÅ]+|\W+")
                                               (map #(or (second %) (first %)))
                                               (split-with (complement #{"/"})))
        prefix (butlast before-slash)
        alt-1 (last before-slash)]
    (->> [(concat prefix [alt-1] suffix)
          (concat prefix [alt-2] suffix)]
         (map str/join)
         (map str/trim))))

(def custom-labels
  "Explicit label alternatives for labels that can't be easily split"
  {"Kvalitetsingenjör/-tekniker, elkraft" ["Kvalitetsingenjör, elkraft"
                                           "Kvalitetstekniker, elkraft"]
   "Film/redigering-Media 100"            ["Film-media 100"
                                           "Redigering-media 100"]
   "Mark/infrastruktur-AutoCAD Civil 3D"  ["Mark-AutoCAD Civil 3D"
                                           "Infrastruktur-AutoCAD Civil 3D"]
   "Mark/infrastruktur-AutoCAD Map 3D"    ["Mark-AutoCAD Map 3D"
                                           "Infrastruktur-AutoCAD Map 3D"]
   "Yrkes-/karaktärsämneslärare"          ["Yrkeslärare"
                                           "Karaktärsämneslärare"]})

(def split-concat-labels
  "Labels that will use split-concat"
  #{"Unit tester, metod/systemutvecklingsverktyg"
    "Prosit/Raindance, redovisningssystem"
    "Mästarbrev, broderi/konstbroderi"
    "Gesällbrev, broderi/konstbroderi"
    "Mästarbrev, köttyrket i detaljhandeln/köttmästare"
    "Gesällbrev, köttyrket i detaljhandeln/köttmästare"
    "Samordning/Granskning-Autodesk Navisworks Manage"
    "Samordning/Granskning-Autodesk Navisworks Simulate"
    "Samordning/Granskning-Autodesk Navisworks Review"
    "Samordning/Granskning-Autodesk Navisworks Freedom"
    "Samordning/Granskning-Autodesk Design Review"
    "Samordning/Granskning-Autodesk Buzzsaw"
    "Svets-/lödarmetoder med verifierad kunskap"
    "Avdelningschef, vårdavdelng/mottagning"
    "Design/Visualisering-Autodesk 3ds Max"
    "Design/Visualisering-Autodesk 3ds Max Design"
    "Design/Visualisering-Autodesk Alias"
    "Design/Visualisering-Rhinoceros"
    "Film/redigering-Adobe Premiere"
    "Film/effekter-Adobe After Effects"
    "Film/redigering-Apple Final Cut Pro"
    "Film/redigering-Avid Media Composer"
    "Film/redigering-Lightworks"
    "Film/effekter- Autodesk Flint/Flame/Inferno"
    "Film/redigering-Vegas"
    "Film/effekter-The Foundry Nuke"
    "Film/effekter-eyeon Fusion"
    "Film/effekter-Autodesk Combustion"
    "Film/effekter-Motion"
    "Film/effekter-Autodesk Cleaner"
    "Film/redigering-Autodesk Smoke"
    "Film/redigering-SpeedEDIT"
    "Film/redigering-DaVinci Resolve"
    "Tankerman, olja/kem"
    "Hi-fi/Radio/Video/TV"
    "Webbdesign-Adobe Animate/Flash"
    "Analys/Beräkning-FEM-Design"
    "Analys/Beräkning-Ansys"
    "Analys/Beräkning-Nastran"
    "Analys/Beräkning-MathLab"
    "Analys/Beräkning-Autodesk Robot Structural Analysis"
    "Analys/Beräkning-Robot Millenium"
    "Analys/Beräkning-Autodesk Ecotect Analysis"
    "Analys/Beräkning-VIP-Energy"
    "Analys/Beräkning-WIN-Statik"
    "Analys/Beräkning-Bentley Building Analysis-Design"
    "Mästarbrev, modellsnickare/tekniker"
    "IKT/IT-inriktning, undervisning"
    "Kanslichef/verksamhetschef, församling"})

(def blacklist-labels
  "Exact labels to skip"
  #{"Europa/ej EU/EES/EURES"
    "Europa/EU/EES/EURES"
    "Basle-City / -Town"
    "Basle-Country / -Land"
    "Grafiska tjänster före tryckning (prepress/premedia)"
    "Mästarbrev, gravör (blankstick/silver/skylt-stans och stämpelgravör)"
    "Lastbilsmonterad kran < 18 ton/m"
    "Lastbilsmonterad kran > 18 ton/m"
    "PL/1, programmeringsspråk"
    "Tcl/Tk, programmeringsspråk"
    "PCL/5, kommunikationsprotokoll"
    "VM/CMS, operativsystem"
    "SAP R/3, affärssystem"
    "Synon/2E, programmeringsspråk"
    "TCP/IP, kommunikationsprotokoll"
    "OS/400, operativsystem"
    "OS/390, operativsystem"
    "EN 287-1, 111 MMA, Metallbågsvetsning/ISO 9696-1"
    "EN 287-1, 114 Innershield, Bågsvetsning/ISO 9606-1"
    "EN 287-1, 12 Pulverbågsvetsning/ISO 9606-1"
    "EN 287-1, 131 MIG-svetsning/ISO 9606-1		"
    "EN 287-1, 135 MAG-svetsning/ISO 9606-1"
    "EN 287-1, 136 MAG-svetsning med slaggande rörelektrod/ISO 9606-1"
    "EN 287-1, 141 TIG-svetsning/ISO 9606-1"
    "EN 287-1, 15 Plasmabågsvetsning/ISO 9606-1"
    "EN 13133, Hårdlödning/ISO 13585"
    "EN 287-1, 311 Gassvetsning/ISO 9606-1"
    "EN 1418, Robotsvetsning/ISO 14732, Mekaniserad svetsning"
    "EN 287-1, 138 MAG-svetsning med metallpulverfylld rörelektrod/ISO 9606-1"
    "EN 287-1, 131 MIG-svetsning/ISO 9606-1"
    "Styr/400, logistiksystem"
    "Redovisning/2, redovisningssystem"
    "Microsoft Certified Professional + Internet/MCP + I"
    "Microsoft Certified System Engineer + Internet/MCSE + I"
    "Vision 80/20, telefonväxel"
    "FoxPro/xBase, programmeringsspråk"
    "Gesällbrev, skomakare (beställning/nytillverkning)"
    "Mästarbrev, skomakare (beställning/nytillverkning)"
    "Övrig/ospec forskarutbildning"
    "Lärarutbildning i yrkesämne och praktiskt/estetiskt ämne"
    "Pedagogik och lärarutbildning, övrig/ospec utbildning"
    "Konst och media, övrig/ ospec utbildning"
    "Humaniora, övrig/ospec inriktning"
    "Samhälls- och beteende vetenskap, övrig/ospec inriktning"
    "Journalistik och information, övrig/ospec utbildning"
    "Företagsekonomi, handel, administration, övrig/ospec utb."
    "Biologi och miljövetenskap, övrig/ospec utbildning"
    "Fysik, kemi och geovetenskap, övrig/ospecificerad utbildning"
    "Matematik och naturvetenskap, övrig/ospec utbildning"
    "Data, övrig/ospec utbildning"
    "Teknik och teknisk industri, övrig/ospec inriktning"
    "Tillverkning av trä-, pappers-, glas/porslin- och plastprodukter"
    "Tillverkning, övrig/ospec inriktning"
    "Samhällsbyggnad och byggnadsteknik, övrig/ospec inriktning"
    "Lantbruk, trädgård, skog och fiske, övrig/ospec utbildning"
    "Hälso- och sjukvård, övrig/ospec utbildning"
    "Socialt arbete och omsorg, övrig/ospec utbildning"
    "Personliga tjänster, övrig/ospec utbildning"
    "Miljövård och miljöskydd, övrig/ospec utbildning"
    "Säkerhetstjänster, övrig/ospec utbildning"
    "Övrig/ospec förskoleutbildning"
    "Övrig/ospec förgymnasial utbildning kortare än 9 år"
    "Övrig/ospec förgymnasial utbildning, 9 (10) år"
    "Gymn. utb. kortare än två år, teoretisk/studieförb, ej slutbetyg"
    "Gymnasial utbildning kortare än två år, teoretisk/studieförb."
    "Gymnasial utb. två år, teoretisk/studieförb. - ej slutbetyg"
    "Gymnasial utbildning två år, teoretisk/studieförb."
    "Gymnasial utb. tre år, teoretisk/studieförb, ej slutbetyg"
    "Gymnasial utbildning tre år, teoretisk/studieförb."
    "Eftergymn. utb. kortare än två år - ej universitet/högskola"
    "Eftergymn. utb. två år - ej universitet/högskola"
    "Eftergymn. utb. tre år - ej universitet/högskola"
    "Eftergymn. utb. fyra år - ej universitet/högskola"
    "Eftergymn. utb. minst fem år - ej universitet/högskola"
    "Cuboid (handha utrustn./t.ex.inställn.av ram)"
    "Motorsport/sport"
    "Mästarbrev, hem- o konsumentelektronik(Radio/TV)"
    "Gesällbrev, hem- o konsumentelektronik(Radio/TV)"
    "Tillverkningsindustri-Pro/ENGINEER"
    "Gesällbrev, smides-/konstsmidesyrket"
    "Mästarbrev, smides-/konstsmidesyrket"
    "HTTP/HTTPS, kommunikationsprotokoll"})

(def re-substring-blacklist
  "Fuzzy labels to skip, using regexes"
  #{"(ö|Ö)vrig/(\\s)?ospec"})

(defn- show-alternatives []
  (let [overrides (->> split-concat-labels
                       (map (juxt identity split-concat))
                       (into custom-labels))
        re-substring-blacklist (re-pattern (str ".*(" (str/join "|" re-substring-blacklist) ").*"))]
    (->> (d/q '{:find [?id ?label]
                :keys [:concept/id :concept/preferred-label]
                :where [[?c :concept/id ?id]
                        [?c :concept/preferred-label ?label]
                        [(.contains ^String ?label "/")]
                        [(missing? $ ?c :concept/alternative-labels)]]}
              (conn/get-db))
         (remove #(blacklist-labels (:concept/preferred-label %)))
         (remove #(re-matches re-substring-blacklist (:concept/preferred-label %)))
         (map (fn [{:concept/keys [preferred-label] :as concept}]
                (assoc concept :concept/alternative-labels
                               (or (overrides preferred-label)
                                   (default-split preferred-label))))))))

(defn- patch! []
  (d/transact (conn/get-conn)
              {:tx-data (->> (show-alternatives)
                             (map #(select-keys % [:concept/id
                                                   :concept/alternative-labels]))
                             (cons {:db/id "datomic.tx"
                                    :daynote/comment "Split pref labels to alt labels"}))}))

(comment

  ;; inspect suggested alternatives
  (show-alternatives)

  ;; perform the migration
  (patch!)

  ,)
