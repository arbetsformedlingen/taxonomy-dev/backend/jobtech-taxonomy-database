(ns jobtech-taxonomy-database.patches.language_uri_patch
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [datomic.client.api :as d]
            [jobtech-taxonomy-database.datomic-connection :as conn]
            [jobtech-taxonomy-common.db-schema :as db-schema]))


(def all-languages
  (with-open [reader (io/reader "resources/languages-urls.csv")]
    (doall
      (csv/read-csv reader))))


(defn add-uris [languages]
  (map (fn [[id _ _ iso-uri glottolog-uri q-id]]
         ;; not all rows contains q-id, empty values represented as empty strings
         (if (empty? q-id)
           {:db/id                                   [:concept/id id]
            :concept.external-standard/iso-uri       iso-uri
            :concept.external-standard/glottolog-uri glottolog-uri}

           {:db/id                                   [:concept/id id]
            :concept.external-standard/iso-uri       iso-uri
            :concept.external-standard/glottolog-uri glottolog-uri
            :concept.external-standard/wikidata-uri  q-id}))
       languages))


(defn patch []
  ;; jobtech-taxonomy-common dependency needs to be up to date
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Adding new attributes for external URIs."}]
                              db-schema/schema)})
  (d/transact (conn/get-conn)
              {:tx-data (into [{:db/id           "datomic.tx"
                                :daynote/comment "Lägger till externa URI:er för begreppet."}]
                              (add-uris (next all-languages)))}))

