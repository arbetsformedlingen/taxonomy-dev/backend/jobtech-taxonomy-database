(ns jobtech-taxonomy-database.patches.sun-keyword-patch
  (:gen-class)
  (:require
    [dk.ative.docjure.spreadsheet :as dox]
    [datomic.client.api :as d]
    [jobtech-taxonomy-database.datomic-connection :as conn]
    [jobtech-taxonomy-database.converters.nano-id-assigner :as nano]
    [jobtech-taxonomy-database.types :as t]
    [jobtech-taxonomy-common.relation :as relation]))

;; patch creates keyword concepts and relates them to specific sun field concepts, as specified by SCB


(defn load-book []
  (dox/load-workbook "resources/sun-code-keyword.xlsx"))

(def book (memoize load-book))

(defn load-sheet [sheet-name]
  (dox/select-sheet sheet-name (book)))

(defn load-new-keywords []
  (dox/select-columns {:A :preferred-label :C :sun-concept-id} (load-sheet "new-keyword")))

(defn load-existing-keywords []
  (dox/select-columns {:A :keyword-id :E :sun-concept-id} (load-sheet "existing-keyword")))

(defn create-keyword [preferred-label]
  (let [concept-id (nano/get-nano "keyword" preferred-label)]
    {:db/id                                            concept-id,
     :concept/id                                       concept-id,
     :concept/type                                     t/keyword,
     :concept/definition                               preferred-label
     :concept/preferred-label                          preferred-label}))

(defn create-related-relation [kw-id sun-id]
  (relation/edge-tx sun-id t/related kw-id))

;; remove some unwanted characters
(defn clean-string [s]
  (clojure.string/trim (clojure.string/replace s #" " "")))

(defn create-concepts-and-relations [row]
  (let [keyword (create-keyword (clean-string (:preferred-label row)))
        relations (create-related-relation (:concept/id keyword) (:sun-concept-id row))]
    (concat [keyword] [relations])))

(defn add-relation-to-existing-concepts [row]
  (create-related-relation (:keyword-id row) (:sun-concept-id row)))

(defn patch-sun-keyword []
  (d/transact (conn/get-conn)
              {:tx-data (mapcat create-concepts-and-relations (rest (remove nil? (load-new-keywords))))})

  (d/transact (conn/get-conn)
              {:tx-data (map add-relation-to-existing-concepts (rest (load-existing-keywords)))}))