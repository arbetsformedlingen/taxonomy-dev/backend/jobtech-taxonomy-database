# jobtech-taxonomy-database

Taxonomy database management utilities for JobTech

## ENV
    Add an environment variable named "jobtechtaxonomy" to "PROD" if you want to use the prod datomic

## Creating fresh database

Use `./create-database.sh dev|prod|frontend`

## Adding schema to a fresh database

Use `./setup-database.sh jobtech-taxonomy-...`

## Installation

Download a copy of the legacy database here:
https://gitlab.com/af-group/ams-taxonomy-backup
Follow the instructions in the readme to start local SQL database server

## License

Copyright © 2020 JobTech

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
